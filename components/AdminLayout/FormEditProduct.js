import { Form, Input, Select } from "antd";
import TextArea from "antd/lib/input/TextArea";
const { Option } = Select;
import React from "react";
// import dynamic from "next/dynamic";
//DARI MBAKK Finn
// import MarkdownIt from "markdown-it";
// import "/lib/index.css";

// const MdEditor = dynamic(() => import(""), {
//   ssr: false,
// });

const FormEditProduct = ({ editForm }) => {
  // const mdParser = new MarkdownIt(/* Markdown-it options */);
  // const editorRef = React.useRef(null);

  // const [editorValue, setEditorValue] = React.useState("");

  const onEditorChange = (e) => {
    setEditorValue(e.text);
    editForm.formEdit.setFieldsValue({ description: e.text });
    console.log(e.text, "value from wysiwyg");
  };

  return (
    <div className="my-5">
      <Form
        form={editForm.formEdit}
        layout="vertical"
        onFinish={editForm.onFinishEdit}
      >
        <Form.Item label="Category" name="category">
          <Select
            value={editForm.category}
            onChange={editForm.onChangeCategory}
          >
            <Option value="729006e4-0a9b-40a5-8e65-e0bab714c1ac">
              Main Menu
            </Option>
            <Option value="c4bef283-49dd-4081-9c61-88a45666495f">Snacks</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Menu Name" name="menu">
          <Input value={editForm.menu} onChange={editForm.onChangeMenu} />
        </Form.Item>
        <Form.Item label="Price" name="price">
          <Input value={editForm.price} onChange={editForm.onChangePrice} />
        </Form.Item>
        <Form.Item label="Description" name="description">
          <TextArea
            value={editForm.description}
            onChange={editForm.onChangeDescription}
          />
          {/* <MdEditor
            ref={editorRef}
            value={editorValue}
            className={"h-[500px]"}
            onChange={onEditorChange}
            highlightEnable={true}
            renderHTML={(text) => mdParser.render(text)}
          /> */}
        </Form.Item>
      </Form>
    </div>
  );
};

export default FormEditProduct;
