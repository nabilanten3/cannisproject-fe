import { UserOutlined } from "@ant-design/icons";
import Image from "next/image";
import Logo from "../../public/assets/logo.png";
import { Layout, Col, Row, Space, message } from "antd";
const { Header } = Layout;
import "tailwindcss/tailwind.css";
import Link from "next/link";
import jwt_Decode from "jwt-decode";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const AdminHeader = () => {
  const [userName, setUserName] = useState("");
  const router = useRouter();
  const getTokenJwt = () => {
    try {
      const getToken = localStorage.getItem("access_token");
      const decodedToken = jwt_Decode(getToken);
      setUserName(decodedToken.name);
      console.log(decodedToken);
      if (decodedToken.role !== "Admin") {
        message.warn("You're Cannot Access This Page!");
        router.push("/customer/home");
      }
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getTokenJwt();
  });

  return (
    <Header className=" shadow-md" style={{ background: "white", padding: 0 }}>
      <Row justify="space-between" align="top" className="mx-2">
        <Col>
          <Link href={"/admin/dashboard"}>
            <Image src={Logo} width={100} height={60} alt="logo" />
          </Link>
        </Col>
        <Col>
          <Space>
            <UserOutlined /> {userName}
          </Space>
        </Col>
      </Row>
    </Header>
  );
};

export default AdminHeader;
