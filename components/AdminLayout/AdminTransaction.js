import React, { useEffect, useState } from "react";
import {
  CheckOutlined,
  DeleteOutlined,
  PrinterFilled,
} from "@ant-design/icons";
import {
  Button,
  Table,
  Space,
  Tooltip,
  Select,
  Modal,
  Tag,
  Row,
  Col,
  Input,
} from "antd";
const { Option } = Select;
const { Search } = Input;
import Image from "next/image";
import axios from "axios";

const AdminTransaction = () => {
  const columns = [
    {
      title: "Date",
      render: (value) => value.createdAt.split("T")[0],
      key: "date",
    },
    {
      title: "Name",
      render: (value) => value.user.fullName,
      key: "name",
    },
    {
      title: "Email",
      render: (value) => value.user.email,
      key: "email",
    },
    {
      title: "Phone Number",
      render: (value) => value.user.phone,
      key: "phone",
    },
    { title: "Menu", render: (value) => value.menu.menu, key: "menu" },

    {
      title: "Payment Proof",
      key: "Paymentproof",
      render: (value) => (
        <>
          <Tooltip title="Show Payment Proof">
            <Button type="text" onClick={() => handlePaymentProof(value)}>
              <Tag color="gold">Show Detail</Tag>
            </Button>
          </Tooltip>
        </>
      ),
    },
    {
      title: "Payment Status",
      key: "paymentStatus",
      render: (value) =>
        value.paymentStatus ? (
          <Tag color="green">Confirmed</Tag>
        ) : (
          <Tag color="volcano">Pending</Tag>
        ),
    },
    {
      title: "Delivery Status",
      key: "deliveryStatus",
      render: (value) => {
        if (value.deliveryStatus === "Waiting For Payment") {
          return <Tag color="volcano">Waiting For Payment</Tag>;
        } else if (value.deliveryStatus === "On Delivery") {
          return <Tag color="processing">On Delivery</Tag>;
        } else if (value.deliveryStatus === "Waiting For Approvement") {
          return <Tag color="gold">Waiting For Approvement</Tag>;
        } else {
          return <Tag color="lime">Delivered</Tag>;
        }
      },
    },
    {
      title: "Action",
      key: "action",
      render: (value) => (
        <>
          <Space>
            {value.paymentStatus ? null : ( // <Tag color={"green"}>{value.status}</Tag
              <Tooltip title="Approve Payment">
                <Button
                  icon={<CheckOutlined />}
                  type="link"
                  onClick={() => handleConfirm(value)}
                />
              </Tooltip>
            )}
            <Tooltip title="Decline Transaction">
              <Button
                type="text"
                danger
                onClick={() => handleDelete(value)}
                icon={<DeleteOutlined />}
              />
            </Tooltip>
          </Space>
        </>
      ),
    },
  ];

  const [dataSouce, setDataSource] = useState([]);
  const [image, setImage] = useState();
  const [visible, setVisible] = useState(false);
  const [imageValue, setImageValue] = useState();
  const [totalDataReport, setTotalDataReport] = useState();
  const [exportTransactionData, setExportTransactionData] = useState("");
  const [searchByName, setSearchByName] = useState("");
  const [paymentStatusFilter, setPaymentStatusFilter] = useState(undefined);

  const handlePaymentProof = async (value) => {
    console.log(value, "dari payment PRoof");
    setVisible(true);
    setImageValue(value.image);
    console.log(value.image, "handle");
  };

  const getTransactionData = async (page) => {
    let response = await axios.get(
      `https://api.cannis-catering.online/transactions?page=${page}${
        paymentStatusFilter === undefined
          ? null
          : `&filter.paymentStatus=${paymentStatusFilter}`
      } &search=${searchByName}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      }
    );
    setDataSource(response.data.data);
    console.log(response, "response ");
    setTotalDataReport(response.data.meta.totalItems);
  };

  const handleConfirm = async (value) => {
    const confirmingPayment = {
      paymentStatus: true,
      deliveryStatus: "On Delivery",
    };
    await axios
      .put(
        `https://api.cannis-catering.online/transactions/admin/${value.id}`,
        confirmingPayment,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res, "ini dari res"));
    console.log(value, "ini bukan");
    getTransactionData();
  };

  const handleDelete = async (value) => {
    console.log(value, "dari handle delete");
    await axios
      .delete(`https://api.cannis-catering.online/transactions/${value.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      .then((res) => console.log(res));
    getTransactionData();
  };

  const exportTransaction = async () => {
    await axios
      .get(`https://api.cannis-catering.online/transactions/export/data`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      .then((res) => setExportTransactionData(res.data.filename));
  };

  const handleSearchByName = (value, event) => {
    // console.log(value);
    setSearchByName(value);
  };

  const handleFilterByPaymentStatus = (val) => {
    console.log(val, "ini value");
    setPaymentStatusFilter(val);
  };

  useEffect(() => {
    getTransactionData();
    exportTransaction();
  }, [searchByName, paymentStatusFilter]);
  return (
    <div>
      <Row className="my-5" justify="space-between">
        <Col>
          <Button
            type="primary"
            // icon={}
            className="bg-mainColor"
          >
            <a href={exportTransactionData} target={"_blank"} rel="noreferrer">
              <PrinterFilled /> Export to Excel
            </a>
          </Button>
        </Col>
        <Col>
          <Space>
            <Search
              allowClear
              placeholder="Search Name ..."
              // enterButton
              onSearch={(value, event) => handleSearchByName(value, event)}
            />

            <Select
              placeholder="Payment Status"
              allowClear
              onChange={handleFilterByPaymentStatus}
            >
              <Option value={true} key={true}>
                Confirmed
              </Option>
              <Option value={false} key={false}>
                Waiting For Approvement
              </Option>
            </Select>
          </Space>
        </Col>
      </Row>
      <Table
        dataSource={dataSouce}
        columns={columns}
        pagination={{
          pageSize: 5,
          total: totalDataReport,
          onChange: (page) => {
            getTransactionData(page);
          },
        }}
      />
      <Modal
        visible={visible}
        onOk={() => setVisible(false)}
        centered
        closable
        onCancel={() => setVisible(false)}
      >
        <Image
          width={500}
          height={750}
          src={`https://api.cannis-catering.online/file/${imageValue}`}
          alt="Payment Proof"
        />
      </Modal>
    </div>
  );
};

export default AdminTransaction;
