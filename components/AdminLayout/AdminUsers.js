import { Button, Table, Modal, Space, Input } from "antd";
import React, { useEffect, useRef, useState } from "react";
import { DeleteFilled, SearchOutlined } from "@ant-design/icons";
import axios from "axios";
import Search from "antd/lib/input/Search";

const AdminUsers = () => {
  const [searchByName, setSearchByName] = useState("");
  const [totalDataReport, setTotalDataReport] = useState();

  const columns = (showUserDelete) => {
    return [
      {
        title: "Name",
        dataIndex: "fullName",
        key: "fullName",
        width: "15%",
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email",
        width: "15%",
      },
      {
        title: "Phone Number",
        dataIndex: "phone",
        key: "phone",
        width: "15%",
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
      },
      {
        title: "Action",
        key: "action",
        width: "5%",
        render: (record) => {
          return (
            <Button
              danger
              icon={<DeleteFilled />}
              type="link"
              onClick={() => showUserDelete(record)}
            />
          );
        },
      },
    ];
  };

  const [showDeleteModal, setDeleteModal] = useState(false);
  const [userId, setUserId] = useState("");
  const [users, setUsers] = useState([]);

  const getUsers = async (page) => {
    let response = await axios.get(
      `https://api.cannis-catering.online/users?page=${page}&search=${searchByName}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      }
    );
    console.log(response.data.meta.totalItems);
    setTotalDataReport(response.data.meta.totalItems);
    setUsers(response.data.data);
  };

  useEffect(() => {
    getUsers();
  }, [searchByName]);

  const showUserDelete = (record) => {
    setDeleteModal(true);
    setUserId(record.id);
    // console.log("ini punya userId " + userId);
  };
  const handleCloseModal = () => {
    setDeleteModal(false);
  };

  const handleOk = () => {
    try {
      setDeleteModal(false);
      axios
        .delete(`https://api.cannis-catering.online/users/${userId}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        })
        .then((res) => console.log(res))
        .then(getUsers());
    } catch (e) {
      e.message;
    }
  };

  const handleSearchByName = (value, event) => {
    setSearchByName(value);
    console.log(value);
  };

  return (
    <>
      <Search
        allowClear
        placeholder="Search Name or Email..."
        // enterButton
        onSearch={(value, event) => handleSearchByName(value, event)}
        className="w-56 mt-5"
      />
      <Table
        dataSource={users}
        columns={columns(showUserDelete)}
        pagination={{
          pageSize: 5,
          total: totalDataReport,
          onChange: (page) => {
            getUsers(page);
          },
        }}
        className="my-5"
      />
      <Modal
        title="Delete User"
        visible={showDeleteModal}
        onOk={handleOk}
        onCancel={handleCloseModal}
        okType="danger"
        width={300}
      />
    </>
  );
};

export default AdminUsers;
