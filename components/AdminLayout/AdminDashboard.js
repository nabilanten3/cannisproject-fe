import { Button, Card, Col, Row, Statistic } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { UserOutlined } from "@ant-design/icons";
import Meta from "antd/lib/card/Meta";

const AdminDashboard = () => {
  const [users, setUsers] = useState();
  const [menu, setMenu] = useState();
  const [transaction, setTransaction] = useState();
  const [income, setIncome] = useState("");
  const [exportUserData, setExportUserData] = useState("");
  const [exportTransactionData, setExportTransactionData] = useState("");

  const getSummaryUsers = async () => {
    await axios
      .get("https://api.cannis-catering.online/users", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      .then((res) => setUsers(res.data.meta.totalItems));
    // .then((res) => console.log(res.data.meta.totalItems));
  };

  const getSummaryMenu = async () => {
    await axios
      .get("https://api.cannis-catering.online/menu")
      .then((res) => setMenu(res.data.meta.totalItems));
  };

  const getSummaryTransaction = async () => {
    let response = await axios.get(
      "https://api.cannis-catering.online/transactions",
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      }
    );
    console.log(response.data.items);
    setIncome(response.data.items);
    setTransaction(response.data.meta.totalItems);
  };

  const exportUser = async () => {
    await axios
      .get(`https://api.cannis-catering.online/users/export/data`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      .then((res) => setExportUserData(res.data.filename));
  };

  const exportTransaction = async () => {
    await axios
      .get(`https://api.cannis-catering.online/transactions/export/data`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      .then((res) => setExportTransactionData(res.data.filename));
  };

  useEffect(() => {
    getSummaryUsers();
    getSummaryMenu();
    getSummaryTransaction();
    exportUser();
    exportTransaction();
  }, []);
  return (
    <div className="my-14">
      <Row justify="space-evenly">
        <Col>
          <Card
            hoverable
            className="shadow-lg rounded-md"
            headStyle={{
              color: "white",
              textAlign: "center",
              backgroundColor: "darkblue",
            }}
            style={{
              width: 300,
            }}
            title="Total Users"
          >
            <Statistic
              title="Pengguna Aktif"
              valueStyle={{
                color: "#3f8600",
              }}
              value={`${users} Pengguna`}
            />
            {/* <Button block className="mt-10 ">
              <a href={exportUserData} target={"_blank"} rel="noreferrer">
                Export to Excel
              </a>
            </Button> */}
          </Card>
        </Col>
        <Col>
          <Card
            hoverable
            headStyle={{
              color: "white",
              textAlign: "center",
              backgroundColor: "orange",
            }}
            className="shadow-lg"
            style={{
              width: 300,
            }}
            title="Total Menu"
          >
            <Statistic
              title="Menu Minggu ini"
              valueStyle={{
                color: "#3f8600",
              }}
              value={`${menu} menu`}
            />
          </Card>
        </Col>
        <Col>
          <Card
            hoverable
            headStyle={{
              color: "white",
              textAlign: "center",
              backgroundColor: "green",
            }}
            className="shadow-lg"
            style={{
              width: 300,
            }}
            title="Total Transaksi"
          >
            <Statistic
              title="Transaksi Berhasil"
              valueStyle={{
                color: "#3f8600",
              }}
              value={`${transaction} Transaksi`}
            />
            {/* <Button block className="mt-10 ">
              <a
                href={exportTransactionData}
                target={"_blank"}
                rel="noreferrer"
              >
                Export to Excel
              </a>
            </Button> */}
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default AdminDashboard;
