import React, { useEffect, useState, useRef } from "react";
import {
  Button,
  Space,
  Input,
  Table,
  Row,
  Col,
  Modal,
  message,
  Select,
} from "antd";
const { Search } = Input;
import { DeleteFilled, EditFilled } from "@ant-design/icons";
import axios from "axios";
import FormAddProduct from "./FormAddProduct";
import { useForm } from "antd/lib/form/Form";
import FormEditProduct from "./FormEditProduct";

const AdminProducts = () => {
  const columns = (showModalEdit, showModalDelete) => {
    return [
      {
        title: "Category",
        dataIndex: "category",
        render: (value) => value.category,
      },
      {
        title: "Menu",
        dataIndex: "menu",
        width: "15%",
      },
      {
        title: "Price",
        dataIndex: "price",
        sortDirections: ["descend"],
        sorter: (a, b) => a.price - b.price,
        width: "10%",
      },
      {
        title: "Description",
        dataIndex: "description",
        // width: "60%",
      },
      {
        title: "Action",
        key: "action",
        render: (record) => {
          return (
            <>
              <Button
                type="link"
                icon={<EditFilled />}
                onClick={() => showModalEdit(record)}
              />
              <Button
                type="text"
                danger
                icon={<DeleteFilled />}
                onClick={() => showModalDelete(record)}
              />
            </>
          );
        },
        width: "10%",
      },
    ];
  };

  const [modalDataEdit, setModalDataEdit] = useState({}); //EDIT SESSION
  const [editProductModal, setEditProductModal] = useState(false); //EDIT SESSION
  const [modalIdDelete, setModalIdDelete] = useState(""); //DELETE SESSION
  const [deleteProductModal, setDeleteProductModal] = useState(false); //DELETE SESSION
  const [AddProductModal, setAddProductModal] = useState(false); //ADD SESSION
  const [dataSource, setDataSource] = useState(); //DATA SOURCE
  const [totalDataReport, setTotalDataReport] = useState();
  const [filterByCategory, setFilterByCategory] = useState();
  const [searchByName, setSearchByName] = useState("");

  const [formEdit] = useForm(); //PEMBEDA TIAP FORM
  const [formAdd] = useForm();
  const getMenu = async (page) => {
    try {
      let response = await axios.get(
        `https://api.cannis-catering.online/menu?page=${page}&sortBy=price:DSC${
          filterByCategory === undefined
            ? null
            : `&filter.category.category=${filterByCategory}`
        }&search=${searchByName}`
      );
      setDataSource(response.data.data);
      setTotalDataReport(response.data.meta.totalItems);
      console.log(response);
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getMenu();
  }, [filterByCategory, searchByName]);

  const handleCloseModal = () => {
    setAddProductModal(false);
    setDeleteProductModal(false);
    setEditProductModal(false);
  };

  //ADD SESSION
  const handleAddProduct = () => {
    setAddProductModal(true);
  };

  //DELETE SESSION

  const showModalDelete = (record) => {
    setDeleteProductModal(true);
    setModalIdDelete(record.id);
  };

  const handleOkModalDelete = async () => {
    try {
      setDeleteProductModal(false);
      await axios
        .delete(`https://api.cannis-catering.online/menu/${modalIdDelete}`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        })
        .then((res) => message.success("success delete"));
      console.log(modalIdDelete);
      getMenu();
    } catch (err) {
      message.error("failed delete");
    }
  };

  //EDIT PRODUCT SESSION

  const [menu, setMenu] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");

  const onChangeCategory = (e) => {
    setCategory(e);
    console.log(e);
  };
  const onChangeMenu = (e) => {
    setMenu(e.target.value);
  };
  const onChangePrice = (e) => {
    setPrice(parseInt(e.target.value));
  };
  const onChangeDescription = (e) => {
    setDescription(e.target.value);
  };

  const showModalEdit = (record) => {
    setEditProductModal(true);
    setModalDataEdit(record);
    formEdit.setFieldsValue({
      category: record.category.id,
      menu: record.menu,
      price: record.price,
      description: record.description,
    });
  };

  const onFinishEdit = async () => {
    try {
      const data = formEdit.getFieldsValue();
      await axios
        .put(
          `https://api.cannis-catering.online/menu/edit/${modalDataEdit.id}`,
          {
            ...data,
            category_id: data.category,
          },
          {
            headers: {
              "content-type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("access_token")}`,
            },
          }
        )
        .then(() => {
          message.success("success edit data");
          setEditProductModal(false);
          setModalDataEdit({});
          getMenu();
        });
    } catch (e) {
      e.message;
    }
  };

  const handleFilterByCategory = (e) => {
    console.log(e);
    setFilterByCategory(e);
  };

  const handleSearchByName = (value, event) => {
    console.log(value);
    setSearchByName(value);
  };

  return (
    <>
      <Row justify="space-between" className="my-5">
        <Col>
          <Space>
            <Select
              placeholder="Select By Category"
              allowClear
              onChange={handleFilterByCategory}
            >
              <Select.Option key={"Main Menu"} value="Main Menu">
                Main Menu
              </Select.Option>
              <Select.Option key={"Snacks"} value="Snack">
                Snacks
              </Select.Option>
            </Select>
            <Search
              onSearch={(value, event) => handleSearchByName(value, event)}
              allowClear
              placeholder="Search Menu..."
            />
          </Space>
        </Col>
        <Col>
          <Button
            onClick={handleAddProduct}
            className="bg-mainColor"
            type="primary"
          >
            + Add Product
          </Button>
          s
        </Col>
      </Row>
      <Table
        columns={columns(showModalEdit, showModalDelete)}
        dataSource={dataSource}
        pagination={{
          pageSize: 5,
          total: totalDataReport,
          onChange: (page) => {
            getMenu(page);
          },
        }}
      />
      <Modal
        visible={AddProductModal}
        onOk={handleCloseModal}
        onCancel={handleCloseModal}
        title="Add New Product"
        okText="Save"
      >
        <FormAddProduct addForm={{ getMenu, formAdd }} />
      </Modal>

      <Modal
        visible={deleteProductModal}
        onOk={handleOkModalDelete}
        onCancel={handleCloseModal}
        title="Delete Product"
        okText="Delete"
        okType="danger"
      >
        <p>are You Sure Want to delete This Menu?</p>
      </Modal>

      <Modal
        visible={editProductModal}
        onOk={onFinishEdit}
        onCancel={handleCloseModal}
        title="Edit Product"
        okText="Save Changes"
      >
        <FormEditProduct
          editForm={{
            formEdit,
            onFinishEdit,
            onChangeCategory,
            onChangeMenu,
            onChangePrice,
            onChangeDescription,
            showModalEdit,
            menu,
            category,
            price,
            description,
          }}
        />
      </Modal>
    </>
  );
};

export default AdminProducts;
