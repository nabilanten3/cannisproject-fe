import React, { useEffect, useState } from "react";
import { Row, Col, Avatar, Form, Input, Button, message, Tooltip } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { useForm } from "antd/lib/form/Form";
import jwt_decode from "jwt-decode";
import axios from "axios";
import UploadProfile from "./UploadProfile";

const ContentBar = () => {
  const [email, setEmail] = useState("");
  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [addesss, setAddress] = useState("");
  const [userProfileForm] = useForm();
  const [idUser, setIdUser] = useState("");
  const [profile, setProfile] = useState("");

  const getTokenJwt = async () => {
    try {
      const getToken = localStorage.getItem("access_token");
      const decodedToken = await jwt_decode(getToken);
      const response = await axios.get(
        `https://api.cannis-catering.online/users/${decodedToken.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );
      console.log(response.data.data, "ini dari response");
      setIdUser(response.data.data.id);
      setEmail(response.data.data.email);
      setFullName(response.data.data.fullName);
      setPhone(response.data.data.phone);
      setAddress(response.data.data.address);
      setProfile(response.data.data.image);
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getTokenJwt();
  }, []);

  userProfileForm.setFieldsValue({
    fullName: fullName,
    phone: phone,
    email: email,
    address: addesss,
  });

  const onFinishEditProfile = async (value) => {
    console.log(value);
    const data = userProfileForm.getFieldsValue();
    await axios
      .put(`https://api.cannis-catering.online/users/${idUser}`, data, {
        headers: {
          "content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      })
      // .then((res) => console.log(res))
      .then(message.success("Success Update Profile"))
      .then(getTokenJwt());
  };

  const onChangeImage = async (filepath) => {
    try {
      await axios
        .put(
          `https://api.cannis-catering.online/users/${idUser}`,
          { image: filepath },
          {
            headers: {
              "content-type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("access_token")}`,
            },
          }
        )
        .then((res) => console.log(res, "onChangeImage"))
        .then(getTokenJwt());
    } catch (e) {
      console.log(e.message);
    }
    // console.log(filepath, "isi dari handleChangeImage");
    // setImage(filepath);
  };

  return (
    <>
      <h1 className="text-2xl font-bold">Profile</h1>
      <Row justify="center">
        <Col>
          <Avatar
            size={200}
            icon={<UserOutlined />}
            src={`https://api.cannis-catering.online/file/${profile}`}
          />
        </Col>
      </Row>
      <Row justify="center" className="mt-3">
        <Col>
          <UploadProfile onChangeImage={onChangeImage} />
        </Col>
      </Row>
      <Row>
        <Col span={12} offset={6} style={{ marginTop: 20 }}>
          <Form
            layout="vertical"
            size="large"
            form={userProfileForm}
            onFinish={onFinishEditProfile}
          >
            <Form.Item label="Name" name="fullName">
              <Input />
            </Form.Item>
            <Form.Item label="Phone Number" name="phone">
              <Input />
            </Form.Item>
            <Form.Item label="Email" name="email" rules={[{ type: "email" }]}>
              <Input />
            </Form.Item>
            <Form.Item label="Address" name="address">
              <Input />
            </Form.Item>
            <Button
              style={{ marginRight: 10, marginTop: 20 }}
              type="primary"
              htmlType="submit"
              className="bg-mainColor"
            >
              Save Changes
            </Button>
            <Button>Cancel</Button>
          </Form>
        </Col>
      </Row>
    </>
  );
};

export default ContentBar;
