import { HomeFilled, SettingFilled, LogoutOutlined } from "@ant-design/icons";
import { Button, Layout, Menu, Modal } from "antd";
import React, { useEffect, useState } from "react";
const { Sider } = Layout;
import Link from "next/link";
import jwt_decode from "jwt-decode";
import axios from "axios";
import { useRouter } from "next/router";

function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

const SideBar = () => {
  const items = [
    getItem(<Link href={"/customer/home"}>Home</Link>, "home", <HomeFilled />),
    getItem("Setting", "setting", <SettingFilled />),
    getItem(
      <Button type="text" danger onClick={() => showConfirm()}>
        Delete Account
      </Button>,
      "deleteAccount",
      <LogoutOutlined style={{ color: "red" }} />
    ),
  ];
  const [collapsed, setCollapsed] = useState(false);
  const [userId, setUserId] = useState("");
  const router = useRouter();

  const getTokenJwt = async () => {
    try {
      const getToken = localStorage.getItem("access_token");
      const decodedToken = await jwt_decode(getToken);
      const response = await axios.get(
        `https://api.cannis-catering.online/users/${decodedToken.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );
      console.log(response.data.data.id, "ini dari response");
      setUserId(response.data.data.id);
    } catch (e) {
      console.log(e.message);
    }
  };

  // const handleDeleteAccount = () => {};
  const showConfirm = () => {
    Modal.confirm({
      title: "Delete Your Account??",
      onOk() {
        try {
          axios
            .delete(`https://api.cannis-catering.online/users/${userId}`, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("access_token")}`,
              },
            })
            .then((e) => console.log(e.message))
            .then(localStorage.clear())
            .then(router.push("/"));
        } catch (e) {
          console.log(e.message);
        }
      },

      onCancel() {
        console.log("Cancel");
      },
    });
  };

  useEffect(() => {
    getTokenJwt();
  }, []);
  return (
    <Sider
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
      theme={"light"}
    >
      <div className="logo" />
      <Menu
        theme="light"
        defaultSelectedKeys={["setting"]}
        mode="inline"
        items={items}
      />
    </Sider>
  );
};

export default SideBar;
