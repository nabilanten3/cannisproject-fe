import React, { useState, useEffect } from "react";
import { Button, Col, message, Modal, Row } from "antd";
import axios from "axios";
import Image from "next/image";
import ReactMarkdown from "react-markdown";
import jwt_decode from "jwt-decode";
import { useRouter } from "next/router";

const DetailMenu = () => {
  const router = useRouter();
  const [userId, setUserId] = useState("");
  const [menus, setMenus] = useState([]);
  const getMenu = async () => {
    try {
      await axios
        .get("https://api.cannis-catering.online/menu?limit=100", {
          headers: {
            "content-type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        })
        .then((res) => setMenus(res.data.data));
      // .then((res) => console.log(res.data));
    } catch (e) {
      console.log(e.message);
    }
  };

  const getTokenJwt = () => {
    const tokenUser = localStorage.getItem("access_token");
    const decodedToken = jwt_decode(tokenUser);
    setUserId(decodedToken.id);
    // console.log(decodedToken);
  };

  useEffect(() => {
    getTokenJwt();
    getMenu();
  }, []);

  return (
    <>
      <div className="mx-3 my-5">
        <Row
          className="my-10"
          gutter={{ xs: 8, sm: 16, md: 24, lg: 32, xl: 16 }}
          // justify="center"
        >
          {menus.map((menu, key) => {
            // console.log(menu);
            const BuyedMenu = {
              menuId: menu.id,
              image: menu.image,
              paymentStatus: false,
              deliveryStatus: "Waiting For Payment",
            };
            const handleBuyProduct = async () => {
              try {
                let response = await axios.post(
                  "https://api.cannis-catering.online/transactions",
                  BuyedMenu,
                  {
                    headers: {
                      "content-type": "application/json",
                      Authorization: `Bearer ${localStorage.getItem(
                        "access_token"
                      )}`,
                    },
                  }
                );
                // console.log(response);
                const transactionId = response.data.data.id;
                localStorage.setItem("transaction_id", transactionId);
                message.loading("Please Wait");
                setTimeout(() => {
                  router.push(`/customer/transaction/${userId}`);
                }, 3500);
              } catch (e) {
                console.log(e.message);
              }
            };
            if (menu.category.id === "729006e4-0a9b-40a5-8e65-e0bab714c1ac") {
              return (
                <Col
                  xxl={6}
                  xl={8}
                  lg={8}
                  md={8}
                  className="hover:scale-105 duration-300"
                  key={key}
                >
                  <Image
                    src={`https://api.cannis-catering.online/file/${menu.image}`}
                    width={400}
                    height={300}
                    alt="menu"
                  />
                  <h1 className="text-2xl font-bold mb-2">{menu.menu}</h1>
                  <p className="text-xl font-medium text-green-600 mb-1">
                    Rp.{menu.price}/Week
                  </p>
                  {/* <p className="text-lg mb-4">
                      Description : <br />
                      <ReactMarkdown>{menu.description}</ReactMarkdown>
                    </p> */}
                  <Button
                    type="primary"
                    onClick={handleBuyProduct}
                    className="bg-mainColor rounded-sm mt-2 hover:scale-105 mr-2"
                  >
                    Buy Now
                  </Button>
                  <Button
                    type="link"
                    onClick={() =>
                      Modal.info({
                        title: "Desctiption",
                        content: (
                          <ReactMarkdown>{menu.description}</ReactMarkdown>
                        ),
                        // centered : true
                        width: 1000,
                      })
                    }
                    className=" rounded-sm mt-2 hover:scale-105"
                  >
                    Details
                  </Button>
                </Col>
              );
            }
          })}
        </Row>
      </div>
    </>
  );
};

export default DetailMenu;
