import React, { useEffect, useState } from "react";
import { Card, Col, Row } from "antd";
import axios from "axios";
import Image from "next/image";

const DetailsSnacks = () => {
  const [menus, setMenus] = useState([]);
  const getMenu = async () => {
    try {
      await axios
        .get("https://api.cannis-catering.online/menu?limit=100")
        .then((res) => setMenus(res.data.data));
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getMenu();
  }, []);

  return (
    <div className="mx-3 my-5">
      <Row
        className="my-10"
        justify="center"
        gutter={{ xs: 8, sm: 16, md: 24, lg: 16 }}
      >
        {menus.map((menu, key) => {
          // console.log("ini dari jsx", menu.category.id);
          if (menu.category.id === "c4bef283-49dd-4081-9c61-88a45666495f") {
            return (
              <Col
                key={key}
                span={4}
                xs={8}
                md={8}
                lg={6}
                xl={6}
                xxl={4}
                className="my-5 duration-200 hover:-translate-y-6"
              >
                <Image
                  src={`https://api.cannis-catering.online/file/${menu.image}`}
                  width={250}
                  height={200}
                  alt="menu"
                />
                <h1 className="text-md font-bold">{menu.menu}</h1>
              </Col>
            );
          }
        })}
      </Row>
    </div>
  );
};

export default DetailsSnacks;
