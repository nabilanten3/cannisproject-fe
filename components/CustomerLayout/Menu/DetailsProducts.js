import React, { useState, useEffect } from "react";
import { Button, Col, message, Modal, Row } from "antd";
import axios from "axios";
import Image from "next/image";
import ReactMarkdown from "react-markdown";

const DetailsProducts = () => {
  const [menus, setMenus] = useState([]);
  const getMenu = async () => {
    try {
      await axios
        .get("https://api.cannis-catering.online/menu?limit=100")
        .then((res) => setMenus(res.data.data));
    } catch (e) {
      console.log(e.message);
    }
  };
  useEffect(() => {
    getMenu();
  }, []);

  return (
    <div className="mx-3 my-5">
      <div className="my-10">
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          {menus.map((menu, key) => {
            const handleBuyProduct = async () => {
              message.warn("You Must Login Before Buy");
            };
            if (menu.category.id === "729006e4-0a9b-40a5-8e65-e0bab714c1ac") {
              return (
                <Col
                  span={8}
                  className="hover:scale-105 duration-300"
                  key={key}
                >
                  <Image
                    src={`https://api.cannis-catering.online/file/${menu.image}`}
                    width={400}
                    height={300}
                    alt="menu"
                  />
                  <h1 className="text-2xl font-bold mb-2">{menu.menu}</h1>
                  <p className="text-xl font-medium text-green-600 mb-1">
                    Rp.{menu.price}/Week
                  </p>
                  {/* <p className="text-lg mb-4">
                      Description : <br />
                      <ReactMarkdown>{menu.description}</ReactMarkdown>
                    </p> */}
                  <Button
                    type="primary"
                    onClick={handleBuyProduct}
                    className="bg-mainColor rounded-sm mt-2 hover:scale-105 mr-2"
                  >
                    Buy Now
                  </Button>
                  <Button
                    type="link"
                    onClick={() =>
                      Modal.info({
                        title: "Desctiption",
                        content: (
                          <ReactMarkdown>{menu.description}</ReactMarkdown>
                        ),
                        // centered : true
                        width: 1000,
                      })
                    }
                    className=" rounded-sm mt-2 hover:scale-105"
                  >
                    Details
                  </Button>
                </Col>
              );
            }
          })}
        </Row>
      </div>
    </div>
  );
};

export default DetailsProducts;
