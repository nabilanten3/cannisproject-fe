import Image from "next/image";
import React from "react";
import { Card, message } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import { useRouter } from "next/router";
import Link from "next/link";
import axios from "axios";
import Background from "../../../public/assets/authBg.png";
import jwt_decode from "jwt-decode";

const LoginContent = () => {
  const router = useRouter();
  const onFinish = async (values) => {
    try {
      const data = { email: values.userEmail, password: values.password };
      await axios
        .post(`https://api.cannis-catering.online/auth/login`, data, {
          headers: { "content-type": "application/json" },
        })
        .then((res) => {
          console.log(res.data.access_token);
          const access_token = res.data.access_token;
          localStorage.setItem("access_token", access_token);
          const decodedToken = jwt_decode(access_token);
          if (res.status === 201 || 200) {
            if (decodedToken.role !== "Admin") {
              message.success("sukses Login");
              router.push("/customer/home");
            } else {
              message.success("sukses Login sebagai Admin");
              router.push("/admin/dashboard");
            }
          }
        });
    } catch (e) {
      message.error("Email atau Password Salah!");
    }
  };

  return (
    <div className="justify-center flex h-screen">
      <Image src={Background} layout="fill" alt="background" />
      <div className="self-center ">
        <Card style={{ width: 350, borderRadius: 10 }}>
          <Form name="normal_login" className="login-form" onFinish={onFinish}>
            <h1 className="text-3xl text-center my-5">Login</h1>
            <Form.Item
              name="userEmail"
              rules={[
                {
                  required: true,
                  message: "Please input your Email!",
                },
                { type: "email" },
              ]}
            >
              <Input
                type={"email"}
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your Password!",
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>

            <Form.Item style={{ textAlign: "center" }} className="text-2xl">
              <Button
                type="default"
                htmlType="submit"
                className="login-form-button w-full"
                style={{ backgroundColor: "green", color: "white" }}
              >
                Log in
              </Button>
              <p className="mt-5 text-xs">
                Do not have an account?
                <Link href={"/auth/register"} style={{ color: "green" }}>
                  Register Here!
                </Link>
              </p>
            </Form.Item>
          </Form>
        </Card>
      </div>
    </div>
  );
};

export default LoginContent;
