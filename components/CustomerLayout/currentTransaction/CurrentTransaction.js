import React from "react";
import PaymentMethods from "./PaymentMethods";
import ShippingAddress from "./ShippingAddress";
import UserShoppingDetails from "./UserShoppingDetails";

const CurrentTransaction = ({ userDetails }) => {
  return (
    <div className="bg-white">
      <ShippingAddress userDetails={userDetails} />
      <UserShoppingDetails userDetails={userDetails} />
      <PaymentMethods userDetails={userDetails} />
    </div>
  );
};

export default CurrentTransaction;
