import React, { useEffect, useState } from "react";
import { Col, message, Row, Space } from "antd";
import jwt_decode from "jwt-decode";
import axios from "axios";

const ShippingAddress = ({ userDetails }) => {
  const [fullName, setFullName] = useState();
  const [address, setAddress] = useState();
  const [phone, setPhone] = useState();

  const getToken = async () => {
    const decodedToken = jwt_decode(localStorage.getItem("access_token"));
    const res = await axios.get(
      `https://api.cannis-catering.online/users/${decodedToken.id}`,
      {
        headers: {
          "content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      }
    );
    setFullName(res.data.data.fullName);
    setAddress(res.data.data.address);
    setPhone(res.data.data.phone);
  };

  useEffect(() => {
    getToken();
  }, []);

  const Shipping = () => {
    if (address === null || address === "") {
      return message.warn("Please go to setting to fill Your Address");
    } else {
      return (
        <Row
          justify="space-between"
          align="middle"
          className="shadow-lg px-8 py-5 mb-8"
        >
          <Col span={3}>
            <Space direction="vertical" size={"middle"}>
              <h3>
                <b>Shipping Address</b>
              </h3>
              <p>{fullName}</p>
              <p>{phone}</p>
            </Space>
          </Col>
          <Col span={16}>
            <p>{address}</p>
          </Col>
          <Col>
            <Space direction="vertical" size={"middle"}>
              <h3>
                <b>Transaction Date</b>
              </h3>
              <p>{userDetails.date}</p>
            </Space>
          </Col>
        </Row>
      );
    }
  };

  return <Shipping />;
};

export default ShippingAddress;
