import React, { useState } from "react";
import { UploadOutlined } from "@ant-design/icons";
import { Button, message, Progress, Upload } from "antd";
import axios from "axios";
import appConfig from "../../../config/app";

const UploadImage = ({ onChangeImage }) => {
  const uploadHandler = async (args) => {
    console.log("masuk sini", args);
    try {
      const formData = new FormData();
      formData.append("file", args.file);

      const processImage = await axios
        .post(`${appConfig.apiUrl}/file/upload`, formData, {
          headers: {
            "content-type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        })
        .then((res) => {
          message.success("berhasil Upload File");
          onChangeImage(res.data.data.filename);
        });
    } catch (e) {
      console.log(e, "apa errornya");
      message.error("Upload failed");
    }
  };

  return (
    <div>
      <Upload
        customRequest={(args) => uploadHandler(args)}
        // multiple={false}
        listType="picture-card"
        showUploadList={false}
      >
        <UploadOutlined />
        {/* <Button>Upload your proof of payment here</Button> */}
        {/* <p>Upload your proof of payment here</p> */}
      </Upload>
    </div>
  );
};

export default UploadImage;
