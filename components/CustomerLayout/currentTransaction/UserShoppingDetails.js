import React from "react";
import { Col, Row } from "antd";

const UserShoppingDetails = ({ userDetails }) => {
  return (
    <>
      <Row className="shadow-lg px-8 py-5 mb-8 ">
        <Col span={6} className="font-bold mb-5">
          <p>Ordered Menu</p>
        </Col>
        <Col span={6} className="font-bold mb-5">
          <p>Time Period</p>
        </Col>
        <Col span={6} className="font-bold mb-5">
          <p>Price</p>
        </Col>
        <Col span={6} className="font-bold mb-5">
          <p>Total</p>
        </Col>
        <Col span={6}>
          <p>{userDetails.menuName}</p>
        </Col>
        <Col span={6}>
          <p>1 week</p>
        </Col>
        <Col span={6}>
          <p>Rp. {userDetails.menuPrice}</p>
        </Col>
        <Col span={6}>
          <p>Rp. {userDetails.menuPrice}</p>
        </Col>
      </Row>
    </>
  );
};

export default UserShoppingDetails;
