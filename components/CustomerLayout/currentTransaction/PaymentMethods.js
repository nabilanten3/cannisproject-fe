import { Button, Steps, Col, message, Row } from "antd";
const { Step } = Steps;
import { CheckCircleFilled } from "@ant-design/icons";
import axios from "axios";
import React, { useEffect } from "react";
import UploadImage from "./UploadImage";
import Countdown from "react-countdown";

const PaymentMethods = ({ userDetails }) => {
  //PAYMENT PROOF
  const onChangeImage = async (filepath) => {
    try {
      const transactions_id = localStorage.getItem("transaction_id");
      const paymentProof = {
        menuId: userDetails.menuId,
        image: filepath,
        statusPayment: false,
        deliveryStatus: "Waiting For Approvement",
      };
      await axios
        .put(
          `https://api.cannis-catering.online/transactions/${transactions_id}`,
          paymentProof,
          {
            headers: {
              "content-type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("access_token")}`,
            },
          }
        )
        .then((res) => console.log(res, "onChangeImage"))
        .then(message.success("Success Upload Your Payment Proof"))
        .then(userDetails.getUser());
    } catch (e) {
      message.error(e.message);
    }
    // console.log(filepath, "isi dari handleChangeImage");
    // setImage(filepath);
  };

  //CANCEL TRANSACTION
  const handleCancelTransaction = async (props) => {
    const transaction_id = localStorage.getItem("transaction_id");
    await axios
      .delete(
        `https://api.cannis-catering.online/transactions/${transaction_id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then((res) => console.log(res))
      .then(message.warn(props))
      .then(localStorage.removeItem("transaction_id"))
      .then(localStorage.removeItem("reviewId"))
      .then(setTimeout(() => userDetails.getUser(), 1500));
  };

  //DELIVERED CONFIRMATION
  const handleDeliveryStatus = async () => {
    const transaction_id = localStorage.getItem("transaction_id");
    const deliveryStatus = {
      deliveryStatus: "Delivered",
    };
    await axios
      .put(
        `https://api.cannis-catering.online/transactions/${transaction_id}`,
        deliveryStatus,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      )
      .then(() => message.success("You have recieved your order"))
      .then(
        setTimeout(() => {
          localStorage.removeItem("transaction_id");
        }, 4000)
      )
      .then(
        setTimeout(() => {
          userDetails.getUser();
        }, 4000)
      );
  };

  const DeliveryStatus = () => {
    let current;
    if (userDetails.statusPayment === false) {
      if (
        userDetails.deliveryStatus === "Waiting For Payment" ||
        "Waiting For Approvement"
      ) {
        current = 0;
      }
    } else {
      if (userDetails.deliveryStatus === "On Delivery") {
        current = 1;
      } else {
        current = 2;
      }
    }
    return (
      <Steps current={current} direction="vertical" size="small">
        {userDetails.deliveryStatus === "Waiting For Approvement" ? (
          <Step title="Waiting For Approvement" />
        ) : (
          <Step title="Waiting For Payment" />
        )}
        <Step title="On Delivery" />
        <Step title="Delivered" icon={<CheckCircleFilled />} />
      </Steps>
    );
  };

  const TransactionStatus = () => {
    let status;
    let color;
    if (userDetails.deliveryStatus === "Waiting For Payment") {
      status = "Waiting For Payment";
      color = "text-red-400";
    } else if (userDetails.deliveryStatus === "Waiting For Approvement") {
      status = "Waiting For Approvement";
      color = "text-yellow-500";
    } else if (userDetails.deliveryStatus === "On Delivery") {
      status = "Approved";
      color = "text-mainColor";
    } else {
      status = "Cancelled";
      color = "text-red-500";
    }
    return <span className={color}>{status}</span>;
  };

  return (
    <>
      <Row className="shadow-lg px-8 py-5 mb-8">
        <Col span={24}>
          <Row justify="space-between" className="my-5 font-bold ">
            <Col span={5}>
              <h1 className="text-lg mb-2">Payment Method</h1>
              <h1>Bank Transfer</h1>
            </Col>
            <Col span={17}>
              <h1 className="text-lg mb-2">
                Transfer to the following account number :
              </h1>
              <h2>BNI BANK : 12123342442</h2>
            </Col>
          </Row>
          <Row justify="space-between" className="my-5 font-medium">
            <Col span={24}>
              <h1>
                Please transfer to the following account number with a
                predetermined amount, no less and no more. The nominal error you
                make is not our responsibility, and then upload your proof of
                payment
              </h1>
            </Col>
          </Row>
          <Row justify="space-between" className="my-10 font-medium">
            <Col span={10}>
              {userDetails.deliveryStatus === "Waiting For Payment" && (
                <div>
                  <div className=" mx-20">
                    <UploadImage onChangeImage={onChangeImage} />
                  </div>
                  <Countdown
                    // onTick={(e) => console.log(e)}
                    date={Date.now() + userDetails.expDate}
                    onComplete={() =>
                      handleCancelTransaction(
                        "Your Transaction Has Been Cancelled"
                      )
                    }
                    renderer={({ hours, minutes, seconds }) => (
                      <span className="font-bold text-lg">
                        Please Make a Payment Before <br />
                        <span className="text-yellow-500  px-20 text-2xl">
                          {hours}:{minutes}:{seconds}
                        </span>
                      </span>
                    )}
                  />
                </div>
              )}
            </Col>
            <Col span={8}>
              <h1 className=" font-bold ">
                Transaction Status : <TransactionStatus />
                <br />
                {userDetails.statusPayment &&
                userDetails.deliveryStatus === "On Delivery" ? (
                  <Button
                    className="bg-mainColor mt-10"
                    type="primary"
                    onClick={() => handleDeliveryStatus()}
                  >
                    Order Received
                  </Button>
                ) : null}
                {userDetails.statusPayment ||
                userDetails.deliveryStatus ===
                  "Waiting For Approvement" ? null : (
                  <Button
                    danger
                    type="primary"
                    className="mt-10"
                    onClick={() =>
                      handleCancelTransaction("Transaction Cancelled")
                    }
                  >
                    Cancel Transcation
                  </Button>
                )}
              </h1>
            </Col>
            <Col span={6}>
              <h1 className="mb-3 font-bold">Delivery Status : </h1>
              {userDetails.deliveryStatus && <DeliveryStatus />}
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default PaymentMethods;
