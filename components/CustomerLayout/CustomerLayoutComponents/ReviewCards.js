import { Avatar, Card, Col, Row, Rate } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";

const ReviewCards = () => {
  const [reviews, setReviews] = useState([]);
  const getReviews = async () => [
    await axios
      .get(`https://api.cannis-catering.online/review`)
      .then((res) => setReviews(res.data.items)),
  ];

  useEffect(() => {
    getReviews();
  }, []);

  console.log(reviews);
  return (
    <div>
      <Row justify="center">
        {reviews.map((review) => {
          return (
            <Col span={6} key={review.id}>
              <h1 className="text-xl py-2 font-medium">
                <Rate defaultValue={5} disabled className="mx-5" />
              </h1>
              <p></p>
            </Col>
          );
        })}
      </Row>
    </div>
  );
};

export default ReviewCards;
