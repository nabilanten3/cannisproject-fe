import React, { useEffect, useState } from "react";
import { ArrowRightOutlined } from "@ant-design/icons";
import Image from "next/image";
import axios from "axios";
import { Button, Col, Row } from "antd";
import { useRouter } from "next/router";
const BigCards = () => {
  const router = useRouter();
  const [menus, setMenus] = useState([]);
  const getMenu = async () => {
    try {
      await axios
        .get("https://api.cannis-catering.online/menu?limit=100")
        .then((res) => setMenus(res.data.data));
    } catch (e) {
      console.log(e.message);
    }
  };

  useEffect(() => {
    getMenu();
  }, []);
  return (
    <>
      <div>
        <Row justify="end" align="middle" gutter={16}>
          <Col span={6}>
            <h1 className="text-5xl text-fontColor font-bold">
              Diet Plan Menu
            </h1>
          </Col>
          {menus.map((menu, key) => {
            if (menu.category.id === "729006e4-0a9b-40a5-8e65-e0bab714c1ac") {
              return (
                <Col
                  span={6}
                  key={key}
                  className="rounded-sm bg-transparent duration-200 hover:scale-105 "
                >
                  <Image
                    src={`https://api.cannis-catering.online/file/${menu.image}`}
                    width={400}
                    height={300}
                    alt="menu"
                  />
                  <p className="font-bold text-xl mb-5 text-fontColor">
                    {menu.menu}
                  </p>
                </Col>
              );
            }
          })}
          <Button
            type="text"
            className="hover:text-lime-700 hover:scale-105 text-md font-semibold"
            onClick={() => {
              localStorage.getItem("access_token")
                ? router.push("/customer/product")
                : router.push("/menu");
            }}
          >
            See More <ArrowRightOutlined />
          </Button>
        </Row>
      </div>
    </>
  );
};

export default BigCards;
