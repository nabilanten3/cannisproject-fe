import React from "react";
import {
  WhatsAppOutlined,
  MailOutlined,
  InstagramOutlined,
} from "@ant-design/icons";

const LandingPageFooter = () => {
  return (
    <footer className="p-4 bg-fontColor  shadow md:flex md:items-center md:justify-between md:p-6 dark:bg-gray-800">
      <span className="text-sm text-white sm:text-center dark:text-gray-400">
        © 2022
        <a href="#" className="hover:underline">
          CannisCatering™
        </a>
        . All Rights Reserved.
      </span>
      <ul className="flex flex-wrap items-center mt-3 text-sm  text-white sm:mt-0">
        <li>
          <a href="#" className="mr-4 hover:underline md:mr-6">
            Instagram
          </a>
        </li>
        <li>
          <a href="#" className="mr-4 hover:underline md:mr-6">
            Email
          </a>
        </li>
        <li>
          <a
            href="https://wa.me/6289657793880?text=Saya Ingin nanya"
            target={"_blank"}
            rel="noreferrer"
            className="hover:underline"
          >
            WhatsApp
          </a>
        </li>
      </ul>
    </footer>
  );
};

export default LandingPageFooter;
