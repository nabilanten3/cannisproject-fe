import React from "react";
import Image from "next/image";

const SnacksCards = (props) => {
  return (
    <div className="flex items-center justify-center">
      <div className="max-w-sm overflow-hidden rounded-sm bg-transparent duration-200 hover:scale-105 ">
        <div className="p-1">
          <Image src={props.UrlImage} width={300} height={200} alt="menu" />
          <p className="text-lg font-bold mb-5 text-gray-700">{props.title}</p>
          <p>{props.description}</p>
        </div>
      </div>
    </div>
  );
};

export default SnacksCards;
