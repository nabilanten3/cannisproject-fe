import { React, useContext, useEffect, useState } from "react";
import { Menu, Dropdown, Space, Badge } from "antd";
import { ShoppingCartOutlined, DownOutlined } from "@ant-design/icons";
import Image from "next/image";
import Router, { useRouter } from "next/router";
import Link from "next/link";
import "tailwindcss/tailwind.css";
import logo from "../../../public/assets/logo.png";

const Navigation = () => {
  const items = [
    { label: <Link href={"/"}>Home</Link>, key: "home" },
    { label: <Link href={"/menu"}>Menu</Link>, key: "menu" },
    { label: <Link href={"/auth/login"}>Login/Register</Link> },
  ];

  return (
    <div className="flex justify-between ">
      <Image
        src={logo}
        width={130}
        height={70}
        className="basis-1/6"
        alt="logo"
      />
      <Menu
        items={items}
        mode="horizontal"
        className="basis-5/6 justify-end bg-amber-50"
      />
    </div>
  );
};

export default Navigation;
