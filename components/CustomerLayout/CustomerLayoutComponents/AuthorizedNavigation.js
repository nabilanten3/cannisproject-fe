import { React, useEffect, useState } from "react";
import { Menu, Dropdown, Space, Badge, Button, message } from "antd";
import { ShoppingCartOutlined, DownOutlined } from "@ant-design/icons";
import Image from "next/image";
import { useRouter } from "next/router";
import Link from "next/link";
import "tailwindcss/tailwind.css";
import logo from "../../../public/assets/logo.png";
import jwt_decode from "jwt-decode";
import axios from "axios";

const AuthorizedNavigation = () => {
  const [logged, setLogged] = useState();
  const [username, setUsername] = useState("");
  const [userId, setUserId] = useState("");
  const [selectedKeys, setSelectedKeys] = useState("");
  const router = useRouter();

  const getTokenJwt = async () => {
    try {
      const getToken = localStorage.getItem("access_token");
      const decodedToken = jwt_decode(getToken);

      setLogged(true);

      setUserId(decodedToken.id);
      // console.log(decodedToken);
      const res = await axios.get(
        `https://api.cannis-catering.online/users/${decodedToken.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );
      setUsername(res.data.data.fullName);
      // console.log(res.data.data.fullName);
    } catch (e) {
      // console.log(e.message);
      message.warn("Please Login First!");
      router.push("/auth/login");
    }
  };

  useEffect(() => {
    getTokenJwt();
    const pathNow = router.pathname.split("/")[2];
    setSelectedKeys(pathNow);
    // console.log("masuk sin");
  }, [router.pathname]);

  const handleLogout = () => {
    localStorage.clear();
    message.success("Success Logout");
    setTimeout(() => {
      router.push("/");
    }, 2000);
  };

  const menu = (
    <Menu
      items={[
        {
          key: "Transaction",
          label: (
            <Link href={`/customer/transaction/${userId}`}>Transaction</Link>
          ),
        },
        {
          key: "setting",
          label: <Link href={`/customer/setting/${userId}`}>Setting</Link>,
        },
        {
          key: "logout",
          label: (
            <Button danger onClick={() => handleLogout()} type="primary" block>
              Logout
            </Button>
          ),
        },
      ]}
    />
  );

  const items = [
    { label: <Link href={"/customer/home"}>Home</Link>, key: "home" },
    { label: <Link href={"/customer/product"}>Menu</Link>, key: "product" },
    {
      label: logged ? (
        <Dropdown overlay={menu} trigger={["hover"]}>
          <Space>
            <p>{username}</p> <DownOutlined />
          </Space>
        </Dropdown>
      ) : (
        <Link href={"/auth/login"}>Login</Link>
      ),
    },
  ];

  return (
    <div className="flex justify-between ">
      <Image
        src={logo}
        width={130}
        height={70}
        className="basis-1/6"
        alt="logo"
      />
      <Menu
        items={items}
        mode="horizontal"
        className="basis-5/6 justify-end bg-amber-50"
        defaultSelectedKeys={"home"}
        selectedKeys={[selectedKeys]}
        onClick={(arg) => setSelectedKeys(arg.key)}
      />
    </div>
  );
};

export default AuthorizedNavigation;
