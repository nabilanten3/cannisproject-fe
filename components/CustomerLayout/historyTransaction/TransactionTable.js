import { Button, Form, message, Modal, Rate, Select, Table, Tag } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import { useRouter } from "next/router";
import TextArea from "antd/lib/input/TextArea";

const TransactionTable = () => {
  const columns = [
    {
      key: "date",
      title: "Date",
      render: (value) => value.createdAt.split("T")[0],
    },
    { key: "name", title: "Name", render: (value) => value.user.fullName },
    { key: "email", title: "Email", render: (value) => value.user.email },
    { key: "phone", title: "Phone", render: (value) => value.user.phone },
    { key: "menu", title: "Menu", render: (value) => value.menu.menu },
    {
      key: "totalPrice",
      title: "Total Price",
      render: (value) => <span>Rp. {value.total} </span>,
    },
    {
      key: "transactionStatus",
      title: "Transaction Status",
      render: (value) => Status(value),
      // value.status === "Done" ? (
      //   <Tag color="lime">{value.status}</Tag>
      // ) : (
      //   <Tag color="error">{value.status}</Tag>
      // ),
    },
    {
      // title: "Give Rating",
      key: "rate",
      render: (value) =>
        value.status === "Done" ? (
          <Button onClick={() => handleRate(value)}>Rate</Button>
        ) : (
          <Button onClick={() => router.push("/customer/product")}>
            Buy Again
          </Button>
        ),
    },
    {
      key: "invoice",
      render: (value) =>
        value.status === "Done" ? (
          <a
            href={`/customer/transaction/invoice/${value.id}`}
            target={"_blank"}
            rel="noreferrer"
            onClick={() => handleInvoice(value)}
          >
            Print Invoice
          </a>
        ) : null,
    },
  ];

  const router = useRouter();
  const [dataSource, setDataSource] = useState([]);
  const [visible, setVisible] = useState(false);
  const [rate, setRate] = useState("");
  const [desc, setDesc] = useState("");
  const [transactionId, setTransactionId] = useState("");
  const [transactionStatus, setTransactionStatus] = useState(undefined);
  const [totalDataReport, setTotalDataReport] = useState(null);

  const Status = (value) => {
    let status = <Tag color="blue">On Progress</Tag>;
    if (value.status === "Done") {
      status = <Tag color="lime">{value.status}</Tag>;
    } else if (value.status === "Cancelled") {
      status = <Tag color="error">{value.status}</Tag>;
    }

    return status;
  };

  const getUser = async (page) => {
    const userToken = localStorage.getItem("access_token");
    const decodedToken = jwtDecode(userToken);
    let historyTransactionResponse = await axios.get(
      `https://api.cannis-catering.online/users/history/${
        decodedToken.id
      }?page=${page}${
        transactionStatus === undefined
          ? null
          : `&filter.status=${transactionStatus}`
      }`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      }
    );
    console.log(historyTransactionResponse.data.data, "dari history");
    setDataSource(historyTransactionResponse.data.data);
    setTotalDataReport(historyTransactionResponse.data.meta.totalItems);
  };

  useEffect(() => {
    getUser();
  }, [transactionStatus]);

  const onChangeRate = (e) => {
    console.log(e);
    setRate(e);
  };

  const onChangeDesc = (e) => {
    console.log(e.target.value);
    setDesc(e.target.value);
  };

  const handleRate = (value) => {
    console.log(value.id, "ini dari handlerate");
    setTransactionId(value.id);
    setVisible(true);
  };

  const onFinishedRate = async () => {
    console.log(transactionId, "ini dari finished rate");
    const rateMessage = {
      transaction_id: transactionId,
      message: desc,
      rating: rate,
    };
    try {
      await axios
        .post(`https://api.cannis-catering.online/review/create`, rateMessage, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        })
        .then(
          (res) => message.success("Thank You for Your Feedback")
          // res.status === 200
          //   ? message.success("Thank You for Your Feedback")
          //   : null
        )
        .then(setVisible(false));
    } catch (e) {
      console.log(e.response.status);
      if (e.message === "Network Error") {
        message.error("sambungan anda terputus");
      }
      if (e.response.status === 409) {
        message.error("kamu sudah pernah mereview");
      } else if (e.response.status === 400) {
        message.error("Rating or Message should not be Empty");
      }
    }
  };

  const handleInvoice = (value) => {
    console.log(value);
    localStorage.setItem("transactionId", value.id);
  };

  const handleFilterByStatus = (e) => {
    setTransactionStatus(e);
    console.log(e);
  };
  return (
    <div>
      <div className="my-5 w-52">
        <Select
          placeholder="Transaction Status"
          allowClear
          onChange={handleFilterByStatus}
        >
          <Select.Option value={"Done"} key="done">
            Done
          </Select.Option>
          <Select.Option value={"Cancelled"} key="cancelled">
            Cancelled
          </Select.Option>
        </Select>
      </div>
      <Table
        dataSource={dataSource}
        pagination={{
          total: totalDataReport,
          onChange: (page) => {
            getUser(page);
          },
        }}
        columns={columns}
      />
      <Modal
        visible={visible}
        onCancel={() => setVisible(false)}
        onOk={() => onFinishedRate()}
        okButtonProps={{ className: "bg-mainColor" }}
        closable
        centered
      >
        <Form layout="vertical" onFinish={onFinishedRate}>
          <Form.Item label="Tell People Your Experience with us!" name="rate">
            <Rate value={rate} onChange={onChangeRate} />
          </Form.Item>
          <Form.Item name="menu" label="Message">
            <TextArea onChange={onChangeDesc} value={desc} />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default TransactionTable;
