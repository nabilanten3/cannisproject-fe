import Image from "next/image";
import React, { useEffect, useState } from "react";
import Background from "../../../public/assets/authBg.png";
import { Card, message } from "antd";
import {
  LockOutlined,
  UserOutlined,
  MailOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import Link from "next/link";
import axios from "axios";
import Router, { useRouter } from "next/router";

const RegisterContent = () => {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState(2);
  const router = useRouter();
  const onFinish = async () => {
    try {
      const data = {
        fullName: fullName,
        email: email,
        phone: phone,
        password: password,
        roleId: role,
      };

      let response = await axios
        .post("https://api.cannis-catering.online/auth/register", data, {
          headers: { "content-type": "application/json" },
        })
        .then((res) => {
          if (res.status === 201 || 200) {
            message.success("sukses registrasi");
            router.push("/auth/login");
          }
        });
    } catch (e) {
      e.message;
    }
  };

  const onChangeName = (e) => {
    setFullName(e.target.value);
    console.log(fullName);
  };
  const onChangeEmail = (e) => {
    setEmail(e.target.value);
    console.log(email);
  };
  const onChangePhone = (e) => {
    setPhone(e.target.value);
    console.log(phone);
  };
  const onChangePassword = (e) => {
    setPassword(e.target.value);
    console.log(password);
  };

  const onChangeRole = (e) => {
    setRole(e.target.value);
    console.log(role);
  };

  return (
    <div className="justify-center flex h-screen">
      <Image src={Background} layout="fill" alt="background" />
      <div className="self-center ">
        <Card style={{ width: 350, borderRadius: 10 }}>
          <Form name="normal_login" className="login-form" onFinish={onFinish}>
            <h1 className="text-3xl text-center">Register</h1>
            <Form.Item>
              <Input type={"hidden"} value={role} onChange={onChangeRole} />
            </Form.Item>
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your Username!",
                },
                { whitespace: true },
                { min: 3 },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
                value={fullName}
                onChange={onChangeName}
              />
            </Form.Item>
            <Form.Item
              name="phonenumber"
              rules={[
                {
                  required: true,
                  message: "Please input your Phone Number!",
                },
              ]}
            >
              <Input
                prefix={<PhoneOutlined className="site-form-item-icon" />}
                value={phone}
                onChange={onChangePhone}
                placeholder="Phone Number"
              />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your Email!",
                },
                { type: "email" },
              ]}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                value={email}
                onChange={onChangeEmail}
                placeholder="Email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your Password!",
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                value={password}
                onChange={onChangePassword}
                placeholder="Password"
              />
            </Form.Item>

            <Form.Item style={{ textAlign: "center" }}>
              <Button
                type="default"
                htmlType="submit"
                className="login-form-button w-full"
                style={{ backgroundColor: "green", color: "white" }}
              >
                Register
              </Button>
              <p className="mt-2 text-xs">
                Already have an account?
                <Link href={"/auth/login"} style={{ color: "green" }}>
                  Login Here!
                </Link>
              </p>
            </Form.Item>
          </Form>
        </Card>
      </div>
    </div>
  );
};

export default RegisterContent;
