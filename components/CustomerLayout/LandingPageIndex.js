import React from "react";
import { Layout } from "antd";
import Head from "next/head";
import "tailwindcss/tailwind.css";
import Navigation from "./CustomerLayoutComponents/Navigation";
import LandingPageFooter from "./CustomerLayoutComponents/LandingPageFooter";

const LandingPageIndex = ({ children }) => {
  return (
    <Layout className=" bg-amber-50 font-sans">
      <Head>
        <title>Cannis Catering</title>
      </Head>
      <Layout className=" bg-transparent mx-28">
        <Navigation />
      </Layout>
      {children}
      <Layout className=" bg-fontColor h-full">
        <LandingPageFooter />
      </Layout>
    </Layout>
  );
};

export default LandingPageIndex;
