import { Col, Row } from "antd";
import React from "react";
import Image from "next/image";
import main from "../../../public/assets/main.png";

const LandingPage = () => {
  return (
    <Row justify="space-between" align="middle" className="text-lg">
      <Col xxl={10} xl={12} lg={12} md={12} sm={12}>
        <h1 className="text-7xl text-mainColor line font-medium pb-2">
          Perfect Foods
        </h1>
        <h3 className="text-4xl font-semibold pb-1">For Perfect Diet</h3>
        <p>Cannis Catering is a Healthy and Diet Catering.</p>
        <p> Cannis Catering serves many plan for your diet or </p>
        <p> healthy food and we make sure your diet program </p>
        <p>will be fun</p>
      </Col>
      <Col xxl={10} xl={12} lg={12} md={12} sm={12}>
        <Image src={main} width={650} height={650} alt="main Pict" />
      </Col>
    </Row>
  );
};

export default LandingPage;
