import { Col, Row } from "antd";
import Image from "next/image";
import React from "react";
import Natural from "../../../public/assets/Natural.png";
import MenuBervariasi from "../../../public/assets/MenuBervariasi.png";
import PorsiPas from "../../../public/assets/PorsiPass.png";

const WhyUsPage = () => {
  return (
    <div className="my-5 text-fontColor text-md font-semibold">
      <h1 className="text-5xl text-center mb-20 text-white font-bold">
        Why Should CannisCatering?
      </h1>
      <Row justify="space-around" className="text-center" gutter={16}>
        <Col>
          <Image alt="logo" src={MenuBervariasi} width={200} height={200} />
          <h1 className="text-2xl text-white font-bold  my-3">Varied Menu</h1>
          <p>
            Enjoy a variety of menus without <br />
            fear bored with the same menu
          </p>
        </Col>
        <Col>
          <Image alt="logo" src={Natural} width={200} height={200} />
          <h1 className="text-2xl text-white font-bold my-3">
            Natural dan Fresh
          </h1>
          <p>
            Made from fresh quality ingredients <br />
            obtained from local farmers
          </p>
        </Col>
        <Col>
          <Image alt="logo" src={PorsiPas} width={200} height={200} />
          <h1 className="text-2xl text-white font-bold my-3">
            Balanced Portion
          </h1>
          <p>
            With the right portion of food <br />
            and balanced nutrition
          </p>
        </Col>
      </Row>
    </div>
  );
};

export default WhyUsPage;
