import { Avatar, Carousel, Col, Rate, Row } from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";

const ThirdLandingPage = () => {
  const [reviews, setReviews] = useState([]);
  const getReviews = async () => [
    await axios
      .get(`https://api.cannis-catering.online/review`)
      .then((res) => setReviews(res.data.items)),
  ];

  useEffect(() => {
    getReviews();
  }, []);

  return (
    <div className="mb-16 my-10">
      <h1 className="text-4xl font-bold text-center mb-16 text-fontColor">
        Diet Warrior Experience with CannisCatering
      </h1>
      <Carousel autoplay dots={false}>
        {reviews.map((review, i) => {
          // console.log(review.transaction.user.image);
          return (
            <div key={i}>
              <Row
                justify="center"
                // style={{ background: "red" }}
                // className="py-10 text-xl"
                gutter={16}
              >
                <Col>
                  <Avatar
                    size={100}
                    // icon={<UserOutlined />}
                    src={`https://api.cannis-catering.online/file/${review.transaction.user.image}`}
                  />
                </Col>

                <Col span={8}>
                  <h1 className="text-2xl py-2 font-bold">
                    {review.transaction.user.fullName}
                    <Rate value={review.rate} disabled className="mx-5" />
                  </h1>
                  <p>{review.message}</p>
                </Col>
              </Row>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
};

export default ThirdLandingPage;
