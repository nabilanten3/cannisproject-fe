/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ["api.cannis-catering.online"],
  },
};

module.exports = nextConfig;
