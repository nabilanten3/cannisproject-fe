import React from "react";
import AdminTransaction from "../../components/AdminLayout/AdminTransaction";
import AdminLayout from "../../components/AdminLayout/AdminLayout";

const Products = () => {
  return (
    <AdminLayout>
      <AdminTransaction />
    </AdminLayout>
  );
};

export default Products;
