import React from "react";
import RegisterContent from "../../components/CustomerLayout/RegisterSection/RegisterContent";

const register = () => {
  return (
    <div>
      <RegisterContent />
    </div>
  );
};

export default register;
