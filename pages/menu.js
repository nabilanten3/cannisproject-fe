import React from "react";
import { Layout } from "antd";
import LandingPageIndex from "../components/CustomerLayout/LandingPageIndex";
import DetailsSnacks from "../components/CustomerLayout/Menu/DetailsSnacks";
import DetailsProducts from "../components/CustomerLayout/Menu/DetailsProducts";

const menu = () => {
  return (
    <LandingPageIndex>
      <Layout className=" bg-transparent">
        <Layout className=" bg-transparent h-full mx-28 my-10">
          <h1 className="font-bold text-5xl my-5 text-fontColor">Diet Plan</h1>
          <DetailsProducts />
          <h1 className="font-bold text-5xl my-5 text-fontColor">
            This Week Menu
          </h1>
          <DetailsSnacks />
        </Layout>
      </Layout>
    </LandingPageIndex>
  );
};

export default menu;
