import React from "react";
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import DetailMenu from "../../../components/CustomerLayout/Menu/DetailMenu";
import { Layout } from "antd";
import DetailsSnacks from "../../../components/CustomerLayout/Menu/DetailsSnacks";

const Menu = () => {
  return (
    <CustomerLayout>
      <Layout className=" bg-transparent">
        <Layout className=" bg-transparent h-full mx-28 my-10">
          <h1 className="font-bold text-5xl my-5 text-fontColor">Diet Plan</h1>
          <DetailMenu />
          <h1 className="font-bold text-5xl my-5 text-fontColor">
            This Week Menu
          </h1>
          <DetailsSnacks />
        </Layout>
      </Layout>
    </CustomerLayout>
  );
};

export default Menu;
