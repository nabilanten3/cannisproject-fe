import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Layout, Button, Result, Tabs } from "antd";
const { TabPane } = Tabs;
import CustomerLayout from "../../../components/CustomerLayout/CustomerLayout";
import axios from "axios";
import CurrentTransaction from "../../../components/CustomerLayout/currentTransaction/CurrentTransaction";
import jwt_decode from "jwt-decode";
import TransactionTable from "../../../components/CustomerLayout/historyTransaction/TransactionTable";

const Transaction = () => {
  const router = useRouter();
  const [transaction, setTransaction] = useState();
  const { userId } = router.query;
  const [deliveryStatus, setDeliveryStatus] = useState();
  const [menuName, setMenuName] = useState();
  const [menuPrice, setMenuPrice] = useState("");
  const [menuId, setMenuId] = useState("");
  const [statusPayment, setStatusPayment] = useState("");
  const [date, setDate] = useState();
  const [idTransaction, setIdTransaction] = useState("");
  const [expDate, setExpDate] = useState("");

  const getUser = async () => {
    try {
      let transaction_id = localStorage.getItem("transaction_id");
      if (!transaction_id) {
        setTransaction(false);
      } else {
        setTransaction(true);
        setIdTransaction(transaction_id);
      }
      let responseCurrentTransaction = await axios.get(
        `https://api.cannis-catering.online/transactions/checkout/${transaction_id}`,
        {
          headers: {
            "content-type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );
      setDeliveryStatus(responseCurrentTransaction.data.data.deliveryStatus);
      setStatusPayment(responseCurrentTransaction.data.data.paymentStatus);
      setMenuName(responseCurrentTransaction.data.data.menu.menu);
      setMenuPrice(responseCurrentTransaction.data.data.menu.price);
      setMenuId(responseCurrentTransaction.data.data.menu.id);
      setDate(responseCurrentTransaction.data.data.createdAt.split("T")[0]);
      setExpDate(
        new Date(responseCurrentTransaction.data.data.expDate).getTime() -
          new Date().getTime()
      );
    } catch (e) {
      console.log(e.message);
      return e;
    }
  };

  useEffect(() => {
    localStorage.getItem("transaction_id") && getUser();
  }, []);

  return (
    <CustomerLayout>
      <Layout className="bg-transparent mx-28 mt-10 px-2">
        <Tabs>
          <TabPane
            tab="Current Transaction"
            key="currentTransaction"
            className="min-h-screen"
          >
            {transaction ? (
              <CurrentTransaction
                userDetails={{
                  getUser,
                  deliveryStatus,
                  menuName,
                  menuPrice,
                  menuId,
                  statusPayment,
                  date,
                  idTransaction,
                  expDate,
                }}
              />
            ) : (
              <Result
                status="info"
                title="No Transaction"
                extra={
                  <Button
                    type="dashed"
                    key="console"
                    onClick={() => router.push("/customer/product")}
                  >
                    Buy Diet Plan
                  </Button>
                }
              />
            )}
          </TabPane>
          <TabPane
            tab="Transaction History"
            key="historyTransaction"
            className="min-h-screen"
          >
            <TransactionTable />
          </TabPane>
        </Tabs>
      </Layout>
    </CustomerLayout>
  );
};

export default Transaction;
