import { Layout, ConfigProvider } from "antd";
const { Content } = Layout;
import { useRouter } from "next/router";
import ContentBar from "../../../components/CustomerLayout/userProfile/ContentBar";
import SideBar from "../../../components/CustomerLayout/userProfile/SiderBar";

const UserProfile = () => {
  const router = useRouter();
  const { userId } = router.query;

  return (
    <>
      <Layout style={{ minHeight: "100vh" }}>
        <Layout className="site-layout">
          <SideBar />
          <Content style={{ margin: "20px 16px" }}>
            <ContentBar />
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default UserProfile;
